# Cellevate Data Visualizer


# Use at your own risk
This is a free-to-use PoC software developed to provide visual graph support for Cellevate data files. As a PoC this software is expected to contain some bugs, and a general lack of warning messages and error logs. The software has not been thoroughly tested and its reliability can not be guaranteed. Use it at your own risk. If any observations in the graphs produced suggests a cause for alarm I would strongly recommend to first confirm the data points' authenticity in the original data file.


# How to Use
1. Run the software.
2. Click the "Upload File" button and chose a .txt data file that follows the same structure as the two provided test files. The software is also able to read .xlsx files with the same structure. Multiple files can be uploaded and switched between using the "Selected File" dropdown.
3. Once the file has uploaded a list of all found quantities will appear and can be selected or deselected. The appearing "Generate Graph" button can be used to create a graph using the selected quantities, and the graph software used allows for zooming, area selecting, saving the graph and more.
Some files, e.g. "test-data.txt", will contain multiple starting and stopping times, and this software will split them up under "Selected Run". There is no way to get multiple such runs in the same graph.
4. The "Save All Selected Graphs" button will save a graph for each selected quantity and for each run if there are multiple. It will also save a single graph containing all selected quantities.


# Notes
When using the "Save All Selected Graphs" button the graphs will be saved inside the "graphs" folder. If you have that folder already open you may need to refresh or reopen it in order to see the newly added graphs.
This was built for Windows 10, 64-bit system, and may require those specifics in order to run.



# Copyright
Creator/owner: Edvin Jakobsson, edvinjakobsson5@gmail.com
Copyright: 3-Clause BSD License
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
