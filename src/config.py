QUANTITY_INFO = {
    "HV_1_PV": {"display_name": "+HV", "default": True},
    "HV_2_PV": {"display_name": "-HV", "default": True},
    "HV_3_PV": {"display_name": "HV3", "default": False},
    "Q1": {"display_name": "Q1", "default": True, "divide_by": 100},
    "Q2": {"display_name": "Q2", "default": False},
    "SCAN_1_POS": {"display_name": "Scan 1 Position", "default": True, "divide_by": 10},
    "SCAN_2_POS": {"display_name": "Scan 2 Position", "default": True, "divide_by": 10},
    "RH": {"display_name": "Relative Humidity", "default": True},
    "T": {"display_name": "Temperature", "default": True},
    "COL_SPEED": {"display_name": f"RPM", "default": True, "divide_by": 10},
}
SPLIT_TAG = "$RT_DIS$"
