from CellevateDataVisualizer import CellevateDataVisualizer


def main():
    my_program = CellevateDataVisualizer()
    my_program.run()


if __name__ == "__main__":
    main()
