import pandas as pd
import matplotlib.pyplot as plt
import os

from config import QUANTITY_INFO, SPLIT_TAG


def create_labels_n_multiply(dataframe, quantities):
    labels = []
    for q in quantities:
        label = f"{QUANTITY_INFO[q]['display_name']}" if q in QUANTITY_INFO else q
        if q in QUANTITY_INFO and QUANTITY_INFO[q].get("divide_by"):
            label = (
                f"{QUANTITY_INFO[q]['display_name']} (x{QUANTITY_INFO[q]['divide_by']})"
            )
            dataframe[q] = dataframe[q].div(QUANTITY_INFO[q].get("divide_by"))
        labels.append(label)
    return labels


def save_graph_to_file(dataframe, quantities, filename):
    labels = create_labels_n_multiply(dataframe, quantities)
    ax = dataframe.plot(x="TimeString", y=quantities, label=labels, figsize=(20, 8))
    ax.set_xlabel("Time")
    ax.figure.savefig(filename)
    plt.close("all")


def _read_file(filepath):
    if filepath.endswith(".txt"):
        return pd.read_csv(open(filepath, "r", encoding="UTF-16"), sep="\t")
    elif filepath.endswith(".xlsx"):
        return pd.read_excel(filepath)
    else:
        print("ERROR! Could not read file!")


class CellevateDataFile:
    def __init__(self, filepath):
        _, self.filename = os.path.split(filepath)
        self.graphs_folder = os.path.join(os.getcwd(), os.path.join("graphs", self.filename))
        self.dfs = self._create_dfs(filepath)
        self.start_end_times = [
            (str(df.iloc[0, 0]), str(df.iloc[-1, 0])) for df in self.dfs
        ]

    def _create_dfs(self, filepath):
        all_data = _read_file(filepath)
        for item in ("Validity", "Time_ms"):
            if item in all_data.columns:
                all_data.pop(item=item)
        self.all_quantities = sorted(
            [v for v in all_data["VarName"].unique() if not v.startswith("$")]
        )
        dfs = []
        for run in self._get_all_sub_dfs(all_data):
            df = run.loc[run["VarName"] == self.all_quantities[0]].copy()
            df.pop("VarName")
            df.pop("VarValue")
            for q in self.all_quantities:
                temp_df = run.loc[run["VarName"] == q].copy()
                temp_df.rename(columns={"VarValue": q}, inplace=True)
                temp_df.pop(item="VarName")
                df = df.join(temp_df.set_index("TimeString"), on="TimeString")
            dfs.append(df)
        return dfs

    def _get_all_sub_dfs(self, all_data):
        split_indices = all_data.index[all_data["VarName"] == SPLIT_TAG].tolist()
        all_sub_dfs = []
        start = 0
        for split in split_indices:
            all_sub_dfs.append(all_data.loc[start:split])
            start = split + 1
        return all_sub_dfs

    def plot_graph(self, df_index, quantities):
        plt.close("all")
        dataframe = self.dfs[df_index].copy()
        labels = create_labels_n_multiply(dataframe, quantities)
        ax = dataframe.plot(x="TimeString", y=quantities, label=labels, figsize=(20, 8))
        ax.set_xlabel("Time")
        plt.show()
        plt.close("all")

    def save_all_graphs(self, quantities):
        if not os.path.exists(self.graphs_folder):
            os.makedirs(self.graphs_folder)
        for i, df in enumerate(self.dfs):
            start_time = str(self.start_end_times[i][0]).replace(":", ";")
            save_folder = os.path.join(self.graphs_folder, start_time)
            if not os.path.exists(save_folder):
                os.makedirs(save_folder)
            save_graph_to_file(
                df.copy(),
                quantities=quantities,
                filename=os.path.join(save_folder, "all_quantities"),
            )
            for q in quantities:
                save_graph_to_file(
                    df.copy(),
                    quantities=[q],
                    filename=os.path.join(save_folder, QUANTITY_INFO[q].get('display_name', q)),
                )
