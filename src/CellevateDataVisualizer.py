import tkinter as tk
from tkinter import filedialog
import os

from config import QUANTITY_INFO
from CellevateDataFile import CellevateDataFile


class CellevateDataVisualizer(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Cellevate Data Visualizer")
        self.loaded_files = {}

        self.canvas = tk.Canvas(self, height=700, width=700, bg="#263D42")
        self.canvas.pack()

        self.frame = tk.Frame(self, bg="white")

        self.save_graphs_button = None
        self.generate_graph_button = None
        self.upload_button = tk.Button(
            self.canvas,
            text="Upload File",
            padx=10,
            pady=8,
            fg="white",
            bg="#263D42",
            command=self._upload_file,
        )

        self.selected_file = tk.StringVar(self, "No files uploaded")
        self.selected_file.trace("w", self._check_file_select)
        self.file_select = tk.OptionMenu(
            self.frame, self.selected_file, value=self.selected_file.get()
        )

        self.selected_run = tk.StringVar(self, "No file selected")
        self.selected_run.trace("w", self._check_run_select)
        self.run_select = tk.OptionMenu(
            self.frame, self.selected_run, value=self.selected_run.get()
        )

        loaded_file_message = tk.Label(
            self.frame,
            text=f"Selected File:",
            fg="black",
            bg="white",
        )
        loaded_file_message.place(x=200, y=0)
        self.file_select.place(x=200, y=20)

        self.frame.place(relwidth=0.8, relheight=0.8, relx=0.1, rely=0.1)

        loaded_file_message = tk.Label(
            self.frame,
            text=f"Selected Run:",
            fg="black",
            bg="white",
        )
        loaded_file_message.place(x=200, y=80)
        self.run_select.place(x=200, y=100)

        self.upload_button.place(x=20, y=15)

    def _upload_file(self):
        filepath, filename = os.path.split(
            filedialog.askopenfilename(
                initialdir=os.getcwd(),
                title="Select File",
                filetypes=(("txt files", "*.txt"), ("all files", "*.*")),
            )
        )
        # TODO CHECK IF FILENAME ALREADY EXIST
        # TODO CHECK IF FILE TYPE/FORMAT IS CORRECT
        self.loaded_files[filename] = CellevateDataFile(
            os.path.join(filepath, filename)
        )

        loaded_file_message = tk.Label(
            self.frame,
            text=f"'{filename}' successfully uploaded!",
            fg="green",
            bg="white",
        )
        loaded_file_message.pack(side=tk.BOTTOM)
        self._reset_file_select_menu(
            options=self.loaded_files.keys(), selected=filename
        )
        if len(self.loaded_files[self.selected_file.get()].start_end_times) > 1:
            loaded_file_message = tk.Label(
                self.frame,
                text=f"WARNING! '{filename}' has multiple starting times!\nYou can change run under 'Selected Run'.",
                fg="red",
                bg="white",
            )
            loaded_file_message.pack(side=tk.BOTTOM)

    def _reset_file_select_menu(self, options, selected):
        menu = self.file_select["menu"]
        menu.delete(0, "end")
        for string in options:
            menu.add_command(
                label=string, command=lambda value=string: self.selected_file.set(value)
            )
        self.selected_file.set(selected)

    def _reset_run_select_menu(self, options, selected):
        menu = self.run_select["menu"]
        menu.delete(0, "end")
        for string in options:
            menu.add_command(
                label=string, command=lambda value=string: self.selected_run.set(value)
            )
        self.selected_run.set(selected)

    def _check_run_select(self, *args):
        if self.selected_run.get() == "No file selected":
            return None
        print(f"The selected run has changed to '{self.selected_run.get()}'")

    def _check_file_select(self, *args):
        if self.selected_file.get() == "No files uploaded":
            return None
        print(f"The selected file has changed to '{self.selected_file.get()}'")

        self._reset_run_select_menu(
            options=self.loaded_files[self.selected_file.get()].start_end_times,
            selected=self.loaded_files[self.selected_file.get()].start_end_times[0],
        )

        if not self.save_graphs_button:
            print("creating graph buttons")
            self.generate_graph_button = tk.Button(
                self.canvas,
                text="Generate Graph",
                padx=10,
                pady=8,
                fg="white",
                bg="#263D42",
                command=self._generate_graph,
            )
            self.save_graphs_button = tk.Button(
                self.canvas,
                text="Save All Selected Graphs",
                padx=10,
                pady=8,
                fg="white",
                bg="#263D42",
                command=self._save_graphs,
            )
            self.generate_graph_button.place(x=270, y=15)
            self.save_graphs_button.place(x=490, y=15)

            print("Creating quantities checkboxes")
            quantities = self.loaded_files[self.selected_file.get()].all_quantities
            print(f"quantities: {quantities}")
            self.selected_quantities = {}
            y_pos = 200
            for q in quantities:
                self.selected_quantities[q] = tk.IntVar()
                if QUANTITY_INFO[q].get("default"):
                    self.selected_quantities[q].set(1)
                chk = tk.Checkbutton(
                    self.frame,
                    text=QUANTITY_INFO[q].get("display_name", q),
                    variable=self.selected_quantities[q],
                )
                chk.place(x=0, y=y_pos)
                y_pos += 25

    def _save_graphs(self):
        self.loaded_files[self.selected_file.get()].save_all_graphs(
            [q for q, v in self.selected_quantities.items() if v.get()]
        )

    def _generate_graph(self):
        print("Generating graph..")
        run_list = [
            str(time)
            for time in self.loaded_files[self.selected_file.get()].start_end_times
        ]
        selected_run_index = run_list.index(self.selected_run.get())
        self.loaded_files[self.selected_file.get()].plot_graph(
            df_index=selected_run_index,
            quantities=[q for q, v in self.selected_quantities.items() if v.get()],
        )

    def run(self):
        self.mainloop()
